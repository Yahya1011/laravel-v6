<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'IndexController@index');
Route::get('/form', 'FormController@bio');
Route::post('/welcome', 'IndexController@kirim');
Route::get('/data-table', 'IndexController@dataTables');
