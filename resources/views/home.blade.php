@extends('layout.master')
@section('judul')
Buat Account Baru 
@endsection
@section('content')
<form action="/welcome" method="POST">
    @csrf
    
       <p>First Name :</p>
       <input type="text" name="firstName" placeholder="First Name">

       <p>Last Name :</p>
       <input type="text" name="lastName" placeholder="Last Name">

       <p>Genre:</p>
       <input type="radio" id="male" name="genre" value="Male">
       <label for="male">Male</label><br>
       <input type="radio" id="female" name="genre" value="Female">
       <label for="female">Female</label><br>

       <p>Nationality</p>
       <select name="nationality" id="nationality">
           <option value="Indonesia">Indonesia</option>
           <option value="Inggris">Inggris</option>
       </select>

       <p>Language Spoken</p>
       <input type="checkbox" id="indonesia">
       <label for="indonesia">Bahasa Indonesia</label><br>
       <input type="checkbox" id="english">
       <label for="english">English</label><br>
       <input type="checkbox" id="other">
       <label for="other">Other</label>

       <p>Bio</p>
       <textarea name="text" id="" cols="30" rows="10" placeholder="masukkan bio anda disini"></textarea><br><br>
       
       <input type="submit" value="Sign In">
</form>
@endsection
    
</body>
</html>