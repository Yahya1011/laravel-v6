@extends('layout.master')
@section('judul')
Media Online
@endsection

@section('content')
<h3>Sosial Media Developer</h3>
<p>Belajar dan berbagi agar hidup lebih baik</p>
<h3>Benefit join di Media Online</h3>
<ul>
    <li>Medapatkan motovasi dari sesama para Developer</li>
    <li>Sharing Knowlenge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>
<h3>Cara bergabung ke Media Online</h3>
<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan diri di <a href="/form">Form Sign Up</a></li>
    <li>Selesai</li>
</ol>
@endsection
