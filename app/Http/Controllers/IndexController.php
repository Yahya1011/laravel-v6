<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        return view('beranda');
    }

    public function kirim(Request $request)
    {
        //dd($request->all());
        $fname = $request['firstName'];
        $lname = $request['lastName'];

        return view('register', compact('fname', 'lname'));
    }

    public function dataTables()
    {
        return view('data-tables');
    }
}
